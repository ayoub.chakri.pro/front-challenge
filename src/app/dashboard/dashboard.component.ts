import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {

  storages: any[] = [
    {
      name:"Drive",
      brand:"assets/brand/drive.png",
      max:50,
      min:20,
      value:40
    },
    {
      name:"Dropbox",
      brand:"assets/brand/dropbox.png",
      max:50,
      min:45,
      value:90
    },
    {
      name:"Onedrive",
      brand:"assets/brand/onedrive.png",
      max:50,
      min:35,
      value:70
    }
  ];

  folders: any[] = [
    {
      name:"Analitics",
      files_count:"15",
      avatars:[
        { src : 'assets/brand/jack.jpg',color : this.RandomColor()},
        { src : 'assets/brand/john.jpg',color : this.RandomColor()},
        { src : 'assets/brand/george.jpg',color : this.RandomColor()},
        { src : 'assets/brand/jack.jpg',color : this.RandomColor()},
        { src : 'assets/brand/john.jpg',color : this.RandomColor()},
        { src : 'assets/brand/george.jpg',color : this.RandomColor()}
      ]
    },
    {
      name:"Assets",
      files_count:"455",
      avatars:[
        { src : 'assets/brand/george.jpg',color : this.RandomColor()},
        { src : 'assets/brand/jack.jpg',color : this.RandomColor()},
        
      ]
    },
    {
      name:"Marketing",
      files_count:"112",
      avatars:[
        { src : 'assets/brand/george.jpg',color : this.RandomColor()},

      ]
    },
  ];

  titles : String[] = [
    'Name','Membres','Last Modified',''
  ]

  list : any[] = [
    {
      name:'Compitetor analysis template',
      members:'Only you',
      date:'Jul 29, 2019',
      type:'pdf'
    },
    {
      name:'How to template',
      members:'2 members',
      date:'Sep 3, 2019',
      type:'archive'
    },
    {
      name:'analysis',
      members:'analysis',
      date:'Dec 12, 2019',
      type:'video'
    },
    {
      name:'How to template',
      members:'2 members',
      date:'Sep 3, 2019',
      type:'archive'
    },
    {
      name:'book analysis',
      members:'analysis',
      date:'Dec 12, 2019',
      type:'video'
    },
    {
      name:'Compitetor template',
      members:'Only you',
      date:'Jul 29, 2019',
      type:'pdf'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

  RandomColor():string {

    var letters = '0123456789ABCDEF';
    var color = '#';
    
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    
    return color;
  
  }


}
