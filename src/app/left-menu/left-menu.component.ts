import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {

  isStorageCollapsed : boolean = false;
  isfoldersCollapsed : boolean = false;

  topics : any[] = [
    {
      name:"My folders",
      folders: [
        {
          name: "Analitics",
          isCollapsed: true,
          subFolders : [
            'setup',
            'folders',
            'bonus',
          ]
        },
        {
          name: "Assets",
          isCollapsed: true,
          subFolders : [
            'images',
            'docs',
          ]
        },
        {
          name: "Marketing",
          isCollapsed: true,
          subFolders : [
            'archive',
            'researches',
            'problems',
          ]
        }
      ],
      isCollapsed: false
    },
    {
      name:"Recent",
      folders: [
        {
          name: "Archives",
          isCollapsed: true,
          subFolders : [
            'folders',
            'docs',
          ]
        },
      ],
      isCollapsed: true
    },
    {
      name:"Shared with me",
      folders: [
        {
          name: "docs",
          isCollapsed: true,
          subFolders : [
            'folders',
            'archives',
          ]
        },
      ],
      isCollapsed: true
    },
    {
      name:"Starred",
      folders: [
        {
          name: "folders",
          isCollapsed: true,
          subFolders : [
            'archives',
            'docs',
          ]
        },
      ],
      isCollapsed: true
    },
    {
      name:"Trash",
      folders: [],
      isCollapsed: true
    }

  ]



  constructor() { }

  ngOnInit() {
  }

}
