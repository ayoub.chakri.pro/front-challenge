# FrontDevinwebTest

## Requirements
You need [node.js](http://nodejs.org) with [npm](http://npmjs.com) on your machine.

## Installation
This app will install all required dependencies automatically. 
Just start the commands below in the root folder where you stored the package.
```SH
$ npm install
```

## Run Application and start development Server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.